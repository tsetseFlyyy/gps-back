package main

import (
	//"database/sql"
	//"encoding/json"
	//"fmt"
	//"gpsModule/routes"
	//"log"
	"net/http"
	"os"
	//"os/exec"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func main() {

	//data := generateData() 
	//if data == nil {
	//	return
	//}

	//err := insertDataIntoDB(data) 
	//if err != nil {
	//	return
	//}

	port := os.Getenv("PORT")

	if port == "" {
		port = "8001"
	}

	router := gin.New()
	router.Use(gin.Logger())

	router.Use(cors.Default())

	router.GET("", func(c *gin.Context) {
		c.IndentedJSON(http.StatusOK, gin.H{"message": "Connected!"})
	})

	//router.POST("/write", routes.WriteAPI)
	//router.GET("/read", routes.ReadAPI)

	router.Run(":" + port)
}

//func insertDataIntoDB(data []byte) error {
//	// Подключение к базе данных PostgreSQL
//	connStr := "user=postgres dbname=GPSDB password=password host=localhost sslmode=disable"
//	db, err := sql.Open("postgres", connStr)
//	if err != nil {
//		return err
//	}
//	defer db.Close()

//	clearTables(db)

//	// Разбор данных из JSON
//	var buses []map[string]interface{}
//	err = json.Unmarshal(data, &buses)
//	if err != nil {
//		return err
//	}

//	// Выполнение запросов на вставку данных в таблицы
//	for _, bus := range buses {
//		// Вставка данных в таблицу "bus"
//		_, err := db.Exec(`
//			INSERT INTO bus (bus_plate_number, bus_depot_id, bus_depot_name) VALUES ($1, $2, $3) ON CONFLICT DO NOTHING`,
//			bus["busPlateNumber"], bus["busDepotId"], bus["busDepotName"])
//		if err != nil {
//			return err
//		}

//		// Вставка данных в таблицу "route"
//		_, err = db.Exec(`
//			INSERT INTO route (route_name, route_direction_code, route_number) VALUES ($1, $2, $3) ON CONFLICT DO NOTHING`,
//			bus["routeName"], bus["routeDirectionCode"], bus["routeNumber"])
//		if err != nil {
//			return err
//		}

//		// Вставка данных в таблицу "trip", если не существует
//		_, err = db.Exec(`
//			INSERT INTO trip (trip_number) SELECT $1 WHERE NOT EXISTS (SELECT 1 FROM trip WHERE trip_number = $1)`,
//			bus["tripNumber"])
//		if err != nil {
//			return err
//		}

//		// Вставка данных в таблицу "location", если не существует
//		_, err = db.Exec(`
//			INSERT INTO location (lat, lon, speed) SELECT $1, $2, $3 WHERE NOT EXISTS (SELECT 1 FROM location WHERE lat = $1 AND lon = $2)`,
//			bus["lat"], bus["lon"], bus["speed"])
//		if err != nil {
//			return err
//		}

//		// Получение ID автобуса
//		var busID int
//		err = db.QueryRow(`SELECT bus_id FROM bus WHERE bus_plate_number = $1`, bus["busPlateNumber"]).Scan(&busID)
//		if err != nil {
//			return err
//		}

//		// Получение ID маршрута
//		var routeID int
//		err = db.QueryRow(`SELECT route_id FROM route WHERE route_number = $1`, bus["routeNumber"]).Scan(&routeID)
//		if err != nil {
//			return err
//		}

//		// Получение ID поездки
//		var tripID int
//		err = db.QueryRow(`SELECT trip_id FROM trip WHERE trip_number = $1`, bus["tripNumber"]).Scan(&tripID)
//		if err != nil {
//			return err
//		}

//		// Получение ID местоположения
//		var locationID int
//		err = db.QueryRow(`SELECT location_id FROM location WHERE lat = $1 AND lon = $2`, bus["lat"], bus["lon"]).Scan(&locationID)
//		if err != nil {
//			return err
//		}

//		// Вставка данных в таблицу "bus_data"
//		_, err = db.Exec(`
//			INSERT INTO bus_data (bus_id, route_id, trip_id, location_id, angle, distance, create_date, terminal_date, terminal_model, terminal_serial_number, locality_code)
//			VALUES ($1, $2, $3, $4, $5, $6, NOW(), NOW(), $7, $8, $9)`,
//			busID, routeID, tripID, locationID, bus["angle"], bus["distance"], bus["terminalModel"], bus["terminalSerialNumber"], bus["localityCode"])
//		if err != nil {
//			return err
//		}
//	}

//	return nil
//}

//func clearTables(db *sql.DB) {
//	// Delete data from 'bus_data'
//	_, err := db.Exec("DELETE FROM bus_data")
//	if err != nil {
//		log.Fatalf("Failed to clear bus_data: %v", err)
//	}

//	// Delete data from 'bus'
//	_, err = db.Exec("DELETE FROM bus")
//	if err != nil {
//		log.Fatalf("Failed to clear bus: %v", err)
//	}

//	// Delete data from 'route'
//	_, err = db.Exec("DELETE FROM route")
//	if err != nil {
//		log.Fatalf("Failed to clear route: %v", err)
//	}

//	// Delete data from 'trip'
//	_, err = db.Exec("DELETE FROM trip")
//	if err != nil {
//		log.Fatalf("Failed to clear trip: %v", err)
//	}

//	// Delete data from 'location'
//	_, err = db.Exec("DELETE FROM location")
//	if err != nil {
//		log.Fatalf("Failed to clear location: %v", err)
//	}
//}

//func generateData() []byte {
//	cmd := exec.Command("python3", "./script.py")

//	output, err := cmd.CombinedOutput()
//	if err != nil {
//		fmt.Println("Error:", err)
//		return nil
//	}

//	return output
//}
