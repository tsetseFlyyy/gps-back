import random
import json

def generate_random_bus_plate_number():
    # Generates a random bus plate number in the format XXXXX where X can be a digit or an uppercase letter
    return f"{random.randint(100,999)}{chr(random.randint(65,90))}{chr(random.randint(65,90))}"

def generate_random_json():
    busesArray = []
    for i in range(10):
        template = {
            "angle":  79.84536743164062,
            "busDepotId":  272256687,
            "busDepotName": "ТОО УралТехСервис",
            "busId":  275814833,
            "busPlateNumber": "412AQ07",  # to be randomized
            "createDate": "2024-02-12T17:20:37",
            "distance":  0.03663880378007889,
            "lat":  51.2088128,  # to be randomized
            "lon":  51.3769565,  # to be randomized
            "localityCode": "ORAL",
            "recordId":  504332,
            "routeDirectionCode": "A_TO_B",
            "routeId":  300765962,
            "routeName": "Автовокзал (ул. Сырым Датова) - ТД Адал",
            "routeNumber": "20",  # to be randomized
            "speed":  32.68107604980469,
            "terminalDate": "2024-02-12T17:20:37",
            "terminalModel": "AV-S10",
            "terminalSerialNumber": "2106026b",
            "tripId":  7878355,
            "tripNumber":  68
        }

        # Randomizing specific fields
        template['busPlateNumber'] = generate_random_bus_plate_number()
        template['lat'] = round(template['lat'] + random.uniform(-0.001, 0.001), 7)
        template['lon'] = round(template['lon'] + random.uniform(-0.001, 0.001), 7) # Longitude ranges from -180 to  180
        template['routeNumber'] = str(random.randint(1,  100))  # Assuming route numbers range from  1 to  100

        busesArray.append(template)  # Append the dictionary directly

    # Convert the list of dictionaries to a JSON array
    return json.dumps(busesArray, indent=4)

# Generate and print the random JSON entity
print(generate_random_json())
