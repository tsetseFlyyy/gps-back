package routes

import (
	"database/sql"
	"encoding/json"
	//"fmt"
	//"os/exec"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func WriteAPI(c *gin.Context) {
	// Подключение к базе данных PostgreSQL
	connStr := "user=postgres dbname=GPSDB password=password host=localhost sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	// Обновление значений полей lat, lon в таблице location
	_, err = db.Exec(`
		UPDATE location
		SET lat = lat + (RANDOM() *  0.002 -  0.001),
		    lon = lon + (RANDOM() *  0.002 -  0.001)
	`)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.IndentedJSON(http.StatusOK, gin.H{"message": "Latitude and longitude updated successfully"})
}

func ReadAPI(c *gin.Context) {
	// Установка соединения с базой данных PostgreSQL
	db, err := sql.Open("postgres", "user=postgres dbname=GPSDB password=password host=localhost sslmode=disable")
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	// Выполнение запроса к базе данных
	rows, err := db.Query(`
	SELECT
	bd.angle AS angle,
	b.bus_depot_id AS busDepotId,
	b.bus_depot_name AS busDepotName,
	b.bus_id AS busId,
	b.bus_plate_number AS busPlateNumber,
	l.lat AS lat,
	l.lon AS lon,
	r.route_id AS routeId,
	r.route_name AS routeName,
	r.route_number AS routeNumber
	FROM
		bus_data bd
	JOIN
		bus b ON bd.bus_id = b.bus_id
	JOIN
		route r ON bd.route_id = r.route_id
	JOIN
		location l ON bd.location_id = l.location_id;
	`)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	defer rows.Close()

	// Считывание данных из результатов запроса и формирование JSON
	var jsonData []map[string]interface{}
	for rows.Next() {
		var angle float64
		var busDepotId int
		var busDepotName string
		var busId int
		var busPlateNumber string
		var lat float64
		var lon float64
		var routeId int
		var routeName string
		var routeNumber string

		err := rows.Scan(
			&angle,
			&busDepotId,
			&busDepotName,
			&busId,
			&busPlateNumber,
			&lat,
			&lon,
			&routeId,
			&routeName,
			&routeNumber,
		)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		data := map[string]interface{}{
			"angle":          angle,
			"busDepotId":     busDepotId,
			"busDepotName":   busDepotName,
			"busId":          busId,
			"busPlateNumber": busPlateNumber,
			"lat":            lat,
			"lon":            lon,
			"routeId":        routeId,
			"routeName":      routeName,
			"routeNumber":    routeNumber,
		}
		jsonData = append(jsonData, data)
	}

	// Преобразование данных в JSON и отправка клиенту
	jsonBytes, err := json.Marshal(jsonData)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.Data(http.StatusOK, "application/json", jsonBytes)
}

